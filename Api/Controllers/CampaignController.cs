﻿using Api.AuthorizationHelper;
using Api.Data;
using Api.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Controllers
{

    public class Responsible
    {
        public string ID { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string To { get; set; }
        public bool IsNext { get; set; }
        public int Order { get; set; }
    }

    public class ResponsibleInfo
    {
        public string Email { get; set; }
        public int CardId { get; set; }

    }

    public class RequestModel
    {
        public string mobileNumber { get; set; }
        public string Source { get; set; }
    }

    // [BasicAuthorize("base")]
    [ApiController]
    [Route("[controller]")]
    public class CampaignController : ControllerBase
    {
        private readonly grdbcontext context;
        private readonly IEmailSenderService emailSenderService;
        private readonly MarketingDbContext marketingContext;
        private readonly ILogger<CampaignController> _logger;




        public CampaignController(ILogger<CampaignController> logger, grdbcontext context, IEmailSenderService emailSenderService, MarketingDbContext marketingContext)
        {
            _logger = logger;
            this.context = context;
            this.emailSenderService = emailSenderService;
            this.marketingContext = marketingContext;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RequestModel model)
        {

            try
            {
                var mobileNumber = model.mobileNumber;

                if (mobileNumber.Length != 9)
                {
                    return BadRequest("მობილური ნომრის ზომა ან ფორმატი არასწორია");
                }

                var clientState = "normal";
                List<string> tos = new List<string>();

                List<int> bregisterActiveStates = new List<int>();
                bregisterActiveStates.Add(1);
                bregisterActiveStates.Add(4);
                bregisterActiveStates.Add(5);
                bregisterActiveStates.Add(8);

                var existingNumber = marketingContext.campaigns.FirstOrDefault(x => x.mobilenumber == mobileNumber);
                if (existingNumber != null)
                {
                    return Ok("მითითებული ნომერი უკვე მიღებულია, დამატებითი ინფორმაციისთვის დაუკავშირდი ცხელ ხაზს 032 2 444 999");
                }
                var info = context.PolicyInfo.FromSqlRaw("dbo.GetPolicyInfoByPhoneNumber @PhoneNumber = {0}", mobileNumber).ToList();

                if (info == null || info.Count() == 0)
                {

                    //Bregister
                    if (context.BregisterClients.Any(x => bregisterActiveStates.Contains(x.TS) && (x.Phone1.Contains(mobileNumber) || x.Phone2.Contains(mobileNumber))))
                    {

                        var list = context.BregisterClients.Where(x => bregisterActiveStates.Contains(x.TS) && (x.Phone1.Contains(mobileNumber) || x.Phone2.Contains(mobileNumber))).ToList();
                        if (list.Any(x => x.Location == 2246 && x.Agent == null))
                        {

                            tos.Add("mgurgenidze@aldagi.ge");
                        }
                        else if (list.Any(x => x.Agent == 44 && x.TS == 8))
                        {
                            tos.Add("mgurgenidze@aldagi.ge");
                        }
                        else
                        {
                            var agentId = list.FirstOrDefault().Agent;
                            var cardId = context.Agents.FirstOrDefault(x => x.ID == agentId);
                            var card = context.CARDs.FirstOrDefault(x => x.NREFCARD == cardId.Card);
                            if (card == null || string.IsNullOrEmpty(card.Email))
                            {
                                tos.Add("mrusitashvili@aldagi.ge");


                            }
                            else
                            {
                                tos.Add(card.Email);

                            }
                        }
                        CreateAndSend(mobileNumber, clientState, tos, model.Source);
                        return Ok("ნომერი მიღებულია, ჩვენი წარმომადგენელი მალე დაგიკავშირდება");
                    }

                    else
                    {
                        tos.Add("mgurgenidze@aldagi.ge");
                        CreateAndSend(mobileNumber, clientState, tos, model.Source);
                        return Ok("ნომერი მიღებულია, ჩვენი წარმომადგენელი მალე დაგიკავშირდება");
                    }

                }

                var isDirty = false;
                ///თუ ნომრით მოიძებნა ერთზე მეტი კლიენტი
                if (info.GroupBy(x => x.IdNumber).Count() > 1)
                {

                    tos.Add("mgurgenidze@aldagi.ge");
                    isDirty = true;
                    CreateAndSend(mobileNumber, clientState, tos, model.Source);
                    return Ok("ნომერი მიღებულია, ჩვენი წარმომადგენელი მალე დაგიკავშირდება");
                }

                var agroBlackList = context.AgroBlackLists.FirstOrDefault(x => x.IdentificationNumber == info.FirstOrDefault().IdNumber);

                if (agroBlackList != null && !isDirty)
                {
                    clientState = "ბლექლისტი";

                    if (info.Any(x => x.CONTID != null))
                    {
                        tos.Add(info.FirstOrDefault(x => x.CONTID != null).ResponsibleEmail);
                    }
                    else
                    {
                        tos.Add("ntserodze@aldagi.ge");
                        tos.Add("t.chaladze@aldagi.ge");
                    }

                    isDirty = true;

                }


                var motorshortlist = context.ShortListClients.FirstOrDefault(x => x.cndscode == info.FirstOrDefault().IdNumber && x.StateID == 1);
                if (motorshortlist != null && !isDirty)
                {
                    clientState = "შორთლისტი";
                    if (info.Any(x => x.CONTID != null))
                    {
                        tos.Add(info.FirstOrDefault(x => x.CONTID != null).ResponsibleEmail);
                    }
                    else
                    {
                        tos.Add("ntserodze@aldagi.ge");
                        tos.Add("t.chaladze@aldagi.ge");
                    }

                    isDirty = true;
                }

                var motorBlackList = context.ShortListClients.FirstOrDefault(x => x.cndscode == info.FirstOrDefault().IdNumber && x.StateID == 3);
                if (motorBlackList != null && !isDirty)
                {
                    clientState = "ბლექლისტი";
                    if (info.Any(x => x.CONTID != null))
                    {
                        tos.Add(info.FirstOrDefault(x => x.CONTID != null).ResponsibleEmail);
                    }
                    else
                    {
                        tos.Add("ntserodze@aldagi.ge");
                        tos.Add("t.chaladze@aldagi.ge");
                    }

                    isDirty = true;
                }


                var PropertyShortList = context.ShortListClients.FirstOrDefault(x => x.cndscode == info.FirstOrDefault().IdNumber && x.StateID == 11);
                if (PropertyShortList != null && !isDirty)
                {
                    clientState = "შორთლისტი";
                    if (info.Any(x => x.CONTID != null))
                    {
                        tos.Add(info.FirstOrDefault(x => x.CONTID != null).ResponsibleEmail);
                    }
                    else
                    {
                        tos.Add("ntserodze@aldagi.ge");
                        tos.Add("t.chaladze@aldagi.ge");
                    }

                    isDirty = true;
                }

                var PropertyBlackList = context.ShortListClients.FirstOrDefault(x => x.cndscode == info.FirstOrDefault().IdNumber && x.StateID == 13);
                if (PropertyBlackList != null && !isDirty)
                {
                    clientState = "ბლექლისტი";
                    if (info.Any(x => x.CONTID != null))
                    {
                        tos.Add(info.FirstOrDefault(x => x.CONTID != null).ResponsibleEmail);
                    }
                    else
                    {
                        tos.Add("ntserodze@aldagi.ge");
                        tos.Add("t.chaladze@aldagi.ge");
                    }

                    isDirty = true;
                }


                if (isDirty)
                {
                    CreateAndSend(mobileNumber, clientState, tos, model.Source);
                    return Ok("ნომერი მიღებულია, ჩვენი წარმომადგენელი მალე დაგიკავშირდება");
                }


                var date = DateTime.Now.Date;
                var minus14 = date.AddMonths(-14);


                //CLUB 24
                if (info.Any(x => x.PLACEID == 4366922))
                {

                    tos.Add("npantsulaia@aldagi.ge");
                }
                //Standalone Not in Public Service Hall
                else if (info.Any(x => x.LICID == 130 && x.STATE == 0 && x.TODATE >= minus14 && x.IsLoanLinked != true && x.PLACEID != 865498355))
                {
                    var responsible = info.OrderByDescending(x => x.FROMDATE).FirstOrDefault();
                    tos.Add(responsible.ResponsibleEmail);


                }
                //LoanLinked
                else if (info.Any(x => x.LICID == 130 && x.STATE == 0 && x.TODATE >= minus14 && x.IsLoanLinked == true))
                {
                    tos.Add("idarsalia@aldagi.ge");

                }
                //Standalone IN BOG
                else if (info.Any(x => x.LICID == 130 && x.STATE == 0 && x.TODATE >= minus14 && x.IsLoanLinked != true && x.PLACEID != 665498368))
                {

                    var responsible = info.OrderByDescending(x => x.FROMDATE).FirstOrDefault();
                    tos.Add(responsible.ResponsibleEmail);

                }
                //Bank Credo
                else if (info.Any(x => x.STATE == 0 && x.TODATE >= minus14 && x.PLACEID == 3061742))
                {
                    tos.Add("ntserodze@aldagi.ge");
                }
                //Bregister
                else if (context.BregisterClients.Any(x => bregisterActiveStates.Contains(x.TS) && (x.Phone1.Contains(mobileNumber) || x.Phone2.Contains(mobileNumber))))
                {

                    var list = context.BregisterClients.Where(x => bregisterActiveStates.Contains(x.TS) && (x.Phone1.Contains(mobileNumber) || x.Phone2.Contains(mobileNumber))).ToList();
                    if (list.Any(x => x.Location == 2246 && x.Agent == null))
                    {

                        tos.Add("mgurgenidze@aldagi.ge");
                    }
                    else if (list.Any(x => x.Agent == 44 && x.TS == 8))
                    {
                        tos.Add("mgurgenidze@aldagi.ge");
                    }
                    else
                    {
                        var agentId = list.FirstOrDefault().Agent;
                        CARD card = null;
                        var cardId = context.Agents.FirstOrDefault(x => x.ID == agentId);
                        if (cardId != null)
                        {
                            card = context.CARDs.FirstOrDefault(x => x.NREFCARD == cardId.Card);
                        }
                        if (card == null || string.IsNullOrEmpty(card.Email))
                        {
                            tos.Add("mrusitashvili@aldagi.ge");


                        }
                        else
                        {
                            tos.Add(card.Email);

                        }
                    }
                }
                else
                {
                    tos.Add("mgurgenidze@aldagi.ge");

                }

                CreateAndSend(mobileNumber, clientState, tos, model.Source);

                return Ok("ნომერი მიღებულია, ჩვენი წარმომადგენელი მალე დაგიკავშირდება");
            }
            catch (Exception ex)
            {

                return BadRequest("გთხოვთ სცადოთ მოგვიანებით");
            }
        }



        private void CreateAndSend(string mobileNumber, string clientState, List<string> mailTo, string source = "")
        {
            var item = new campaign
            {
                createdon = DateTime.Now,
                id = Guid.NewGuid(),
                mobilenumber = mobileNumber,
                redirectedto = string.Join(',', mailTo),
                source = !string.IsNullOrEmpty(source) ? source : "mzadxar",
                status = clientState,
            };

            marketingContext.campaigns.Add(item);
            marketingContext.SaveChanges();


            emailSenderService.SendEmail(new SendEmailRequestModel("mzadxar@aldagi.ge", mailTo, $"mzadxar : {mobileNumber}", $"mzadxar : {mobileNumber}"));
        }


        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var data = marketingContext.campaigns;
            return Ok(data);
        }
    }
}
