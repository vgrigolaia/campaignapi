﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Api.Services
{
    public class SendEmailRequestModel
    {
        public SendEmailRequestModel(string from, List<string> to, string subject, string body)
        {
            From = from;
            To = to;
            Subject = subject;
            Body = body;
        }

        public string From { get; }
        public List<string> To { get; }
        public string Subject { get; }
        public string Body { get; }
    }

    public interface IEmailSenderService
    {
        void SendEmail(SendEmailRequestModel request);
    }

    public class EmailSenderService : IEmailSenderService
    {
        private readonly IConfiguration _configuration;

        public EmailSenderService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void SendEmail(SendEmailRequestModel request)
        {
            try
            {
                var emailApiEndpoint = _configuration["EmailSender:ApiEndpoint"].ToString();
                var objectContent = JsonConvert.SerializeObject(request);
                var stringContent = new StringContent(objectContent, Encoding.UTF8, "application/json");

                var httpClient = new HttpClient();

                var result = httpClient.PostAsync(emailApiEndpoint, stringContent);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
