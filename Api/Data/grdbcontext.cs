﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Data
{
    [Table("campaign", Schema ="public")]
    public class campaign
    {
        public DateTime createdon { get; set; }
        public string redirectedto { get; set; }
        public string status { get; set; }
        public Guid id { get; set; } = Guid.NewGuid();
        public string source { get; set; }
        public string mobilenumber { get; set; }
    }


    public class MarketingDbContext : DbContext
    {
        public MarketingDbContext(DbContextOptions<MarketingDbContext> options) : base(options) { }

        public DbSet<campaign> campaigns { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


        }
    }

    public class grdbcontext : DbContext
    {

        public grdbcontext(DbContextOptions<grdbcontext> options) : base(options)
        {

        }

        public virtual DbSet<PolicyInfo> PolicyInfo { get; set; }
        public virtual DbSet<AgroBlackList> AgroBlackLists { get; set; }
        public virtual DbSet<ShortListClients> ShortListClients { get; set; }
        public virtual DbSet<BregisterClient> BregisterClients { get; set; }

        public virtual DbSet<Agents> Agents { get; set; }
        public virtual DbSet<CARD> CARDs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PolicyInfo>().HasNoKey();

        }

    }
}
