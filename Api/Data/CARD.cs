﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Data
{
    [Table("CARD", Schema = "dbo")]
    public class CARD
    {
        [Key]
        public int NREFCARD { get; set; }
        public string Email { get; set; }


    }
}
