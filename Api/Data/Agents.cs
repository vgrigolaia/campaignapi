﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Data
{
    [Table("AGENT_AGENTS", Schema = "dbo")]
    public class Agents
    {
        [Key]
        public int ID { get; set; }
        public int Card { get; set; }
    }
}
