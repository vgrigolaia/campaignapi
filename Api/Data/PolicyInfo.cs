﻿using System;

namespace Api.Data
{
    public class PolicyInfo
    {
        public int PLACEID { get; set; }
        public int LICID { get; set; }
        public DateTime FROMDATE { get; set; }
        public DateTime TODATE { get; set; }
        public Int32 STATE { get; set; }
        public string POLICENO { get; set; }
        public bool? IsLoanLinked { get; set; }
        public string ResponsibleEmail { get; set; }

        //პასუხისმგებელი
        public int? CONTID { get; set; }
        //სორსი
        public int? COMID { get; set; }
        //გაყიდვის სეგმენტი
        public int? FinancialInstituteId { get; set; }
        public string IdNumber { get; set; }


    }
}
