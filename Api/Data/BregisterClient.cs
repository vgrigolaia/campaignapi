﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Data
{
    [Table("AGENT_CLIENTS", Schema = "dbo")]
    public class BregisterClient
    {
        public int ID { get; set; }
        public int TS { get; set; }
        [Column("Phone 2")]
        public string Phone2 { get; set; }
        [Column("Phone 1")]
        public string Phone1 { get; set; }
        public int? Agent { get; set; }
        public int Location { get; set; }

    }
}
