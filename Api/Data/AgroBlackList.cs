﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Data
{
    [Table("AgroBlackList", Schema = "dbo")]
    public class AgroBlackList
    {
        public int ID { get; set; }
        public string IdentificationNumber { get; set; }
        public bool? IsActive { get; set; }
    }
}
