﻿namespace Api.Data
{
    public class ShortListClients
    {
        public int ShortListClientsID { get; set; }
        public string cndscode { get; set; }
        public int StateID { get; set; }
    }
}
